#!/bin/bash

echo -e "====================ADVENT OF CODE 2022===================="
echo -e "= = = = = = = = = = = =by Niksa Anic= = = = = = = = = = = =\n"

RED="\033[0;31m"
GREEN="\033[1;32m"
NC="\033[0m"

while getopts "td" options
do
    case $options in
        t)
            test="--test"
            ;;
        d)
            debug="--debug"
            ;;
        *)
            echo "Arguments error! Exiting!"
            exit 0
            ;;
    esac
done

shift $((OPTIND-1))
day=$1
script="Day$day.py"

if [ -z $test ]
then
    echo -e "Test mode...\t\t[${RED}DISABLED${NC}]"
    file="inputs/input$day.txt"
else
    echo -e "Test mode...\t\t[${GREEN}ENABLED${NC}]"
    file="test_inputs/test_input$day.txt"
fi

if [ -z $debug ]
then
    echo -e "Debug mode...\t\t[${RED}DISABLED${NC}]"
else
    echo -e "Debug mode...\t\t[${GREEN}ENABLED${NC}]"
fi

echo -e "Executing...\n[$script $debug $test --file $file]\n"
python3 days/$script $debug $test --file $file