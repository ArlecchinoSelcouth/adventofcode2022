import argparse

def part_one(input_data, debug):
    print("---------------------DAY XX - PART ONE---------------------\n")
    print("\nDAY XX - PART ONE - RESULT - [{result}]\n".format(
        result=1
    ))

    return 1


def part_two(input_data, debug):
    print("---------------------DAY XX - PART TWO---------------------\n")
    print("\nDAY XX - PART TWO - RESULT - [{result}]\n".format(
        result=2
    ))

    return 2


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file")
    parser.add_argument("-d", "--debug", action="store_true")
    parser.add_argument("-t", "--test", action="store_true")
    args = parser.parse_args()

    with open(args.file, "r") as file:
        input_data = file.readlines()

    result_1 = part_one(input_data, args.debug)
    result_2 = part_two(input_data, args.debug)

    if args.test:
        assert(result_1 == 1)
        assert(result_2 == 2)
    
    print("===========================================================\n")
