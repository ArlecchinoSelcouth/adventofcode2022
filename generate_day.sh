number=$1
if [ ! -f days/Day$number.py ]
then
    cp template_day.py days/Day$number.py
    touch test_inputs/test_input$number.txt
    sed "s/XX/$number/g" -i days/Day$number.py
fi