import argparse

def get_calories_list(input_data, debug):
    elves = []
    calories = 0

    for elf in input_data:
        if not elf.strip():
            elves.append(calories)
            if(debug):
                print("[DEBUG] Elf {elf} is carrying {calories} calories!".format(
                    elf=len(elves),
                    calories=calories
                ))
            calories = 0
        else:
            calories += int(elf)

    elves.append(calories)
    if(debug):
        print("[DEBUG] Elf {elf} is carrying {calories} calories!".format(
            elf=len(elves),
            calories=calories
        ))
        
    return elves


def part_one(input_data, debug):
    print("---------------------DAY 01 - PART ONE---------------------\n")

    elves = get_calories_list(input_data, debug)
    most_caloric_elf = max(elves)

    print("\nDAY 01 - PART ONE - RESULT - [{result}]\n".format(
        result=most_caloric_elf
    ))

    return most_caloric_elf


def part_two(input_data, debug):
    print("---------------------DAY 01 - PART TWO---------------------\n")

    elves = get_calories_list(input_data, debug)
    most_caloric_three_elves = 0

    for i in range(3):
        max_elf = max(elves)
        most_caloric_three_elves += max_elf
        elves.remove(max_elf)
        if(debug):
            print("[DEBUG] Maximum calories on elf {elf_num}: {max_elf}".format(
                elf_num=i+1, 
                max_elf=max_elf
            ))
    
    print("\nDAY 01 - PART TWO - RESULT - [{result}]\n".format(
        result=most_caloric_three_elves
    ))

    return most_caloric_three_elves


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file")
    parser.add_argument("-d", "--debug", action="store_true")
    parser.add_argument("-t", "--test", action="store_true")
    args = parser.parse_args()

    with open(args.file, "r") as file:
        input_data = file.readlines()

    result_1 = part_one(input_data, args.debug)
    result_2 = part_two(input_data, args.debug)

    if args.test:
        assert(result_1 == 24000)
        assert(result_2 == 45000)
    
    print("===========================================================\n")
