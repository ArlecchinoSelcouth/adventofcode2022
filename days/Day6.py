import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-f", "--file")
args = parser.parse_args()

def find_marker(buffer_size, marker_name):
    marker_buffer = []
    marker_location = 0
    for character in input.strip():
        if len(marker_buffer) < buffer_size:
            marker_buffer.append(character)
        else:
            # print("TRYING WINDOW", marker_buffer, "\nINDEX IS", marker_location)
            if(all(map(lambda x: marker_buffer.count(x) == 1, marker_buffer))):
                print("ALL CHARACTERS ARE DIFFERENT!")
                print(marker_buffer)
                print("INDEX [" + marker_name + "]:", marker_location, "\n")
                break
            else:
                # print("NO GOOD\n")
                marker_buffer.pop(0)
                marker_buffer.append(character)
        marker_location += 1 

input = open(args.file, "r").readline()
find_marker(4, "sync-marker")
find_marker(14, "stat-of-message-marker")
