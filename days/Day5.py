import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-f", "--file")
args = parser.parse_args()

input = open(args.file, "r").readlines()
# containers
# {
#     1: [a b c ...]
#     2: [c d e ...]
#     ...
#     n: [x y z ...]
# }
containers = {1: [], 2: [], 3: [], 4: [], 5: [], 6: [], 7: [], 8: [], 9: []}
containers2 = {1: [], 2: [], 3: [], 4: [], 5: [], 6: [], 7: [], 8: [], 9: []}
tmp_containers = []
for line in input:
    #step one - read the starting position of crates and generate the stack dictionary
    if not line.startswith("move"):
        if not "1" in line:
            # [  X  ]     [  X  ]     [  X  ]     [  X  ]  ...
            # 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 ...
            # 1 -> 1
            # 2 -> 5
            # 3 -> 9
            # 4 -> 13
            # index / 4 + 1
            # 13 / 4 = 3 + 1 = 4
            # 9 / 4 = 2 + 1 = 3
            # 1 / 4 = 0 + 1 = 1
            for index in range(1, len(line), 4):
                if line[index].isalpha():
                    containers[int(round((index / 4),0)) + 1].insert(0,line[index])
                    containers2[int(round((index / 4),0)) + 1].insert(0,line[index]) 
    #step two - perform actual movements    
    else:
        quantity = int(line.split(" ")[1])
        source = int(line.split(" ")[3])
        destination = int(line.split(" ")[5])
        for crate in range(quantity):
            containers[destination].append(containers[source].pop())
            tmp_containers.append(containers2[source].pop())
        while len(tmp_containers) != 0:
            containers2[destination].append(tmp_containers.pop())

answer = ""
for key in containers.keys():
    print(containers[key])
    answer += containers[key][-1]
print("ANSWER: " + answer + "\n")
answer = ""
for key in containers2.keys():
    print(containers2[key])
    answer += containers2[key][-1]
print("ANSWER 2: " + answer)