import argparse

def part_one(input_data, debug):
    print("---------------------DAY 02 - PART ONE---------------------\n")

    SHAPE_LIBRARY = {
        "X" : "ROCK",
        "Y" : "PAPER",
        "Z" : "SCISSORS",
        "A" : "ROCK",
        "B" : "PAPER",
        "C" : "SCISSORS"
    }

    DRAW_POINTS = 3
    WIN_POINTS = 6
    LOSE_POINTS = 0

    SHAPE_POINTS = {
        "X" : 1,
        "Y" : 2,
        "Z" : 3
    }

    OUTCOMES = {
        ("X", "A") : DRAW_POINTS,
        ("X", "C") : WIN_POINTS,
        ("X", "B") : LOSE_POINTS,
        ("Y", "B") : DRAW_POINTS,
        ("Y", "A") : WIN_POINTS,
        ("Y", "C") : LOSE_POINTS,
        ("Z", "C") : DRAW_POINTS,
        ("Z", "B") : WIN_POINTS,
        ("Z", "A") : LOSE_POINTS
    }

    total_points = 0

    for match in input_data:
        player = match.split(' ')[1].strip()
        opponent = match.split(' ')[0].strip()
        if(debug):
            print("[DEBUG] Processing match: {player} v. {opponent}".format(
                player=SHAPE_LIBRARY[player],
                opponent=SHAPE_LIBRARY[opponent]
            ))
            print("[DEBUG] Points: shape[{shape}] + outcome[{out}]\n".format(
                shape=SHAPE_POINTS[player],
                out=OUTCOMES[(player, opponent)],
            ))

        total_points += SHAPE_POINTS[player]
        total_points += OUTCOMES[(player, opponent)]

    print("\nDAY 02 - PART ONE - RESULT - [{result}]\n".format(
        result=total_points
    ))

    return total_points


def part_two(input_data, debug):
    print("---------------------DAY 02 - PART TWO---------------------\n")
    PLAY_POINTS = {
        "A": 1, 
        "B": 2, 
        "C": 3
    }

    BEATS = {
        "A": "B", 
        "B": "C", 
        "C": "A"
    }
    
    LOSES_TO = {
        "A": "C", 
        "B": "A", 
        "C": "B"
    }

    LOSE_POINTS = 0
    DRAW_POINTS = 3
    WIN_POINTS = 6

    WIN = "Z"
    DRAW = "Y"
    LOSE = "X"

    total_points = 0

    for match in input_data:
        result = match.split(' ')[1].strip()
        opponent = match.split(' ')[0].strip()
        if(debug):
            print("[DEBUG] Processing match against {opponent} with expected outcome {result}".format(
                opponent=opponent,
                result=result
            ))

        if result == DRAW:
            total_points += DRAW_POINTS + PLAY_POINTS[opponent]
        elif result == WIN:
            total_points += WIN_POINTS + PLAY_POINTS[BEATS[opponent]]
        elif result == LOSE:
            total_points += LOSE_POINTS + PLAY_POINTS[LOSES_TO[opponent]]

    print("\nDAY 02 - PART TWO - RESULT - [{result}]\n".format(
        result=total_points
    ))

    return total_points

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file")
    parser.add_argument("-d", "--debug", action="store_true")
    parser.add_argument("-t", "--test", action="store_true")
    args = parser.parse_args()

    with open(args.file, "r") as file:
        input_data = file.readlines()

    result_1 = part_one(input_data, args.debug)
    result_2 = part_two(input_data, args.debug)

    if args.test:
        assert(result_1 == 15)
        assert(result_2 == 12)
    
    print("===========================================================\n")
