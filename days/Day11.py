import argparse
from math import prod

monkey_list = []
k = 0
debug = False


class Monkey:
    def __init__(self, monkey, part):

        self.operation = []
        self.throw = {}
        self.inspections = 0
        self.part = part

        for line in monkey:
            if "Monkey" in line:
                self.name = line.split(" ")[1].split(":")[0]

            if "Starting items" in line:
                self.items = line.split(":")[1].split(",")
                self.items = list(map(lambda item: int(item.strip()), self.items))

            if "Test" in line:
                self.test = int(line.split(" ")[3])

            if "Operation" in line:
                operations = line.split("=")[1].strip().split(" ")
                for o in range(1, len(operations)):
                    if "*" in operations[o]:
                        self.operation.append("MULTIPLY")
                    elif "+" in operations[o]:
                        self.operation.append("ADD")
                    elif "old" in operations[o]:
                        self.operation.append("SELF")
                    else:
                        self.operation.append(int(operations[o]))

            if "false" in line:
                self.throw[False] = int(line.split(" ")[5].strip())
            if "true" in line:
                self.throw[True] = int(line.split(" ")[5].strip())


    def pass_item(self, item):
        self.items.append(item)

    def inspect_items(self):
        self.items.reverse()
        global monkey_list

        if debug:
            print("[DEBUG] Monkey [{name}] item state is: {items}".format(
                name=self.name,
                items=self.items
            ))

        while(self.items):
            item_worry = self.items.pop()
            self.inspections += 1
            if debug:
                print("[DEBUG] Monkey [{name}] is inspecting item [{item}]".format(
                    name=self.name,
                    item=item_worry
                ))

            item_modifier = self.operation[1]
            item_modifier = item_worry if item_modifier == "SELF" else item_modifier
            item_worry = item_worry + item_modifier if \
                            self.operation[0] == "ADD" else \
                            item_worry * item_modifier
            if debug:
                print("[DEBUG] During inspection, after [{op} {mod}] new worry is [{new_worry}]".format(
                    op=self.operation[0],
                    mod=item_modifier,
                    new_worry=item_worry
                ))

            item_worry = item_worry % k if self.part == 2 else int(item_worry / 3)
            if debug:
                print("[DEBUG] After inspection, after [DIVIDE 3] new worry is [{new_worry}]".format(
                    new_worry=item_worry
                ))

            test_results = item_worry % self.test == 0
            next_monkey = self.throw[test_results]
            if debug:
                print("[DEBUG] Test [{worry} % {test}] results are [{results}]! Throwing to Monkey [{next_monkey}]".format(
                    worry=item_worry,
                    test=self.test,
                    results=test_results,
                    next_monkey=next_monkey
                ))
            if debug:
                print()
            monkey_list[next_monkey].pass_item(item_worry)
        if debug:
            print()

    def __str__(self):
        return "\t\tMonkey {name}\n\t\tItems: {items}\n\t\tOperation: {operation}\
            \n\t\tTest: divisble by {test}\n\t\t  On false: {false}\n\t\t  On true: {true}".format(
            name=self.name,
            items=self.items,
            operation=self.operation,
            test=self.test,
            false=self.throw[False],
            true=self.throw[True]
            
        )


def create_monkeys(input_data, part):
    global monkey_list
    monkey_data = []

    for monkey_line in input_data:
        monkey_line = monkey_line.strip()

        if monkey_line:
            monkey_data.append(monkey_line)
        else:
            new_monkey = Monkey(monkey_data, part)
            monkey_list.append(new_monkey)
            if debug:
                print("\n[DEBUG] Created:\n {monke}".format(
                    monke=new_monkey
                ))

            monkey_data = []


def start_monkey_business(rounds):
    monkey_inspections = []
    global debug

    for round in range(rounds):
        if debug:
            print("[DEBUG] -----------Round {round}-----------\n".format(
                round=round,
            ))

        for monkey in monkey_list:
            monkey.inspect_items()

            
    for monkey in monkey_list:

        monkey_inspections.append(monkey.inspections)
        most_active_monkeys = sorted(monkey_inspections, reverse=True)
        if debug:
            print("[DEBUG] Monkey [{name}] item list @ [{insp:3d}] inspects is now: {list}".format(
                name=monkey.name,
                list=monkey.items,
                insp=monkey.inspections
            ))
    if debug:
        print("[DEBUG] Most active monkeys are [{m1}] and [{m2}]".format(
            m1=most_active_monkeys[0],
            m2=most_active_monkeys[1]
        ))

    monkey_business = most_active_monkeys[0] * most_active_monkeys[1]

    return monkey_business


def part_one(input_data, loc_debug):
    print("---------------------DAY 11 - PART ONE---------------------\n")
    global debug
    debug = loc_debug

    create_monkeys(input_data, 1)
    if debug:
        print()

    monkey_business = start_monkey_business(20)

    print("\nDAY 11 - PART ONE - RESULT - [{result}]\n".format(
        result=monkey_business
    ))
    return monkey_business


def part_two(input_data, loc_debug):
    print("---------------------DAY 11 - PART TWO---------------------\n")
    global debug
    debug = loc_debug

    if debug:
        print()

    global k
    k = prod(monkey.test for monkey in monkey_list)

    for monkey in monkey_list:
        monkey.part = 2

    monkey_business = start_monkey_business(10000)

    print("\nDAY 11 - PART TWO - RESULT - [{result}]\n".format(
        result=monkey_business
    ))

    return monkey_business


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file")
    parser.add_argument("-d", "--debug", action="store_true")
    parser.add_argument("-t", "--test", action="store_true")
    args = parser.parse_args()

    with open(args.file, "r") as file:
        input_data = file.readlines()

    result_1 = part_one(input_data, args.debug)
    result_2 = part_two(input_data, args.debug)

    if args.test:
        assert(result_1 == 10605)
        assert(result_2 == 2)
    
    print("===========================================================\n")
