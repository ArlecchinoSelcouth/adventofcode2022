import sys
import argparse

def prmap(map):
    for line in map:
        newline = ""
        for element in line:
            newline += element + " "
        print(newline)

    print()


def f(x):
    if(x == 0):
        return 0
    elif(x > 0):
        return 1
    elif(x < 0):
        return -1

def move_the_head(line, h):
    if "D" in line:
        h[0] += 1
    elif "U" in line:
        h[0] -= 1
    elif "L" in line:
        h[1] -= 1
    elif "R" in line:
        h[1] += 1
    
    return h

def follow(head, tail, debug):

    distance = [head[0]-tail[0], head[1]-tail[1]]
    close = (-1, 0, 1)

    if(all([
            distance[0] in close,
            distance[1] in close
        ])):

        distance[0] = 0
        distance[1] = 0
    else:            
        distance[0] = f(distance[0])
        distance[1] = f(distance[1])

    tail[0] += distance[0]
    tail[1] += distance[1]

    if debug:
        print("[DEBUG] Final state: \n\t\tH[{H0}][{H1}]\n\t\tT[{T0}][{T1}]".format(
            H0=head[0],
            H1=head[1],
            T0=tail[0],
            T1=tail[1]
        ))

def part_one(input_data, debug):
    print("---------------------DAY 09 - PART ONE---------------------\n")

    visited_spots = []

    #generate a bunch of nots on start
    knots = []
    for i in range(2):
        knots.append([0, 0])

    for line in input_data:
        for step in range(int(line.split(" ")[1])):
            for knot in range(len(knots)-1):

                h = knots[knot]
                t = knots[knot + 1]

                if debug:
                    print("[DEBUG] Executing movement [{move}] at step [{step}]".format(
                        move=line.strip(),
                        step=step+1
                    ))
                    print("[DEBUG] Working with knots:\n\t\t{knot1} {knot1_cords}\n\t\t{knot2} {knot2_cords}".format(
                            knot1=knot,
                            knot2=knot + 1,
                            knot1_cords=h,
                            knot2_cords=t,
                    ))

                if knot == 0:
                    h = move_the_head(line, h)
                
                follow(h, t, debug)
                
                if(knot + 1 == len(knots) - 1):
                    if((t[0], t[1]) not in visited_spots):
                        visited_spots.append((t[0], t[1]))
                        if debug:
                            print("[DEBUG] Adding to visited positions: {t}".format(
                                t=t
                            ))
                


    print("\nDAY 09 - PART ONE - RESULT - [{result}]\n".format(
        result=len(visited_spots)
    ))

    return len(visited_spots)

def part_two(input_data, debug):
    print("---------------------DAY 09 - PART TWO---------------------\n")
    visited_spots = []

    knots = []
    for i in range(10):
        knots.append([0, 0])

    for line in input_data:
        for step in range(int(line.split(" ")[1])):
            for knot in range(len(knots)-1):

                h = knots[knot]
                t = knots[knot + 1]

                if debug:
                    print("[DEBUG] Executing movement [{move}] at step [{step}]".format(
                        move=line.strip(),
                        step=step+1
                    ))
                    print("[DEBUG] Working with knots:\n\t\t{knot1} {knot1_cords}\n\t\t{knot2} {knot2_cords}".format(
                            knot1=knot,
                            knot2=knot + 1,
                            knot1_cords=h,
                            knot2_cords=t,
                    ))

                if knot == 0:
                    h = move_the_head(line, h)
                
                follow(h, t, debug)
                
                if(knot + 1 == len(knots) - 1):
                    if((t[0], t[1]) not in visited_spots):
                        visited_spots.append((t[0], t[1]))
                        if debug:
                            print("[DEBUG] Adding to visited positions: {t}".format(
                                t=t
                            ))

    print("\nDAY 09 - PART TWO - RESULT - [{result}]\n".format(
        result=len(visited_spots)
    ))

    return len(visited_spots)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file")
    parser.add_argument("-d", "--debug", action="store_true")
    parser.add_argument("-t", "--test", action="store_true")
    args = parser.parse_args()

    with open(args.file, "r") as file:
        input_data = file.readlines()

    result_1 = part_one(input_data, args.debug)
    result_2 = part_two(input_data, args.debug)

    if args.test:
        assert(result_1 == 88)
        assert(result_2 == 36)
    
    print("===========================================================\n")
