import argparse

def get_ranges(pair, debug):

    range_one = pair.strip().split(",")[0]
    range_two = pair.strip().split(",")[1]
    range_one_start = int(range_one.split("-")[0])
    range_one_end = int(range_one.split("-")[1])
    range_two_start = int(range_two.split("-")[0])
    range_two_end = int(range_two.split("-")[1])
    full_range_one = range(range_one_start, range_one_end+1)
    full_range_two = range(range_two_start, range_two_end+1)

    if len(full_range_one) <= len(full_range_two):
        shorter_range = full_range_one
        longer_range = full_range_two
    else:
        shorter_range = full_range_two
        longer_range = full_range_one
     
    return [longer_range, shorter_range]

def part_one(input_data, debug):
    print("---------------------DAY 04 - PART ONE---------------------\n")

    fully_overlapping_pairs = 0

    for pair in input_data:
        ranges = get_ranges(pair, debug)
        longer_range = ranges[0]
        shorter_range = ranges[1]
        fully_overlapping_pairs += 1 if all(map(lambda x: x in longer_range, shorter_range)) else 0

    print("\nDAY 04 - PART ONE - RESULT - [{result}]\n".format(
        result=fully_overlapping_pairs
    ))

    return fully_overlapping_pairs


def part_two(input_data, debug):
    print("---------------------DAY 04 - PART TWO---------------------\n")

    overlapping_pairs = 0

    for pair in input_data:
        ranges = get_ranges(pair, debug)
        longer_range = ranges[0]
        shorter_range = ranges[1]
        overlapping_pairs += 1 if any(map(lambda x: x in longer_range, shorter_range)) else 0

    print("\nDAY 04 - PART TWO - RESULT - [{result}]\n".format(
        result=overlapping_pairs
    ))

    return overlapping_pairs


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file")
    parser.add_argument("-d", "--debug", action="store_true")
    parser.add_argument("-t", "--test", action="store_true")
    args = parser.parse_args()

    with open(args.file, "r") as file:
        input_data = file.readlines()

    result_1 = part_one(input_data, args.debug)
    result_2 = part_two(input_data, args.debug)

    if args.test:
        assert(result_1 == 2)
        assert(result_2 == 4)
    
    print("===========================================================\n")
