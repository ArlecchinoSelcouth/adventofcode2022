import sys
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-f", "--file")
args = parser.parse_args()


#Folder class
class Folder:
    def __init__(self, name, upfolder):
        self.name: str = name
        self.upfolder: Folder = upfolder
        self.subfolders: Folder = []
        self.files: File = []
        self.size: int = 0
        self.path: str = upfolder.path + "/" + name if upfolder is not None else "/"
    def __str__(self):
        return self.name

    def __eq__(self, other):
        if isinstance(other, Folder) and other.path == self.path:
            return True
        else:
            return False

    def calculate_size(self, size_filter: int):
        for folder in self.subfolders:
            self.size += folder.calculate_size(size_filter)

        for file in self.files:
            self.size += file.size
        isfiltered = "[OK]" if self.size <= size_filter else "[NOT OK]"
        print(isfiltered, "{:9d}".format(self.size), "[", self.path, "]" )
        global total_size
        total_size += self.size if self.size <= size_filter else 0
        return self.size

    # def smallest_compensation(self, excess: int):
    #     for folder in self.subfolders:
            

#File class
class File:
    def __init__(self, size, name):
        self.name: str = name
        self.size: int = size
    
    def __str__(self):
        return self.name


def find_folder(wanted_path, folder_list):
    for folder in folder_list:
        if folder.path == wanted_path:
            return folder
    return None



input = open(args.file, "r").readlines()
debug_level = int(sys.argv[1]) if len(sys.argv) > 1 else 0

root_folder = Folder("/", None)
current_folder = root_folder
first_line = True
for line in input:
    #skip first line because root is already initialized
    if first_line:
        first_line = False
        continue
    line = line.strip()
    if debug_level > 2:
        print(line)

    #if we're moving...
    if line.startswith("$ cd"):
        #...UP, then just move back to upfolder
        if ".." in line:
            if debug_level > 1:
                print("Moving from", current_folder, "to", current_folder.upfolder)
            current_folder = current_folder.upfolder
        #...DOWN, find the folder and move in
        else:
            next_folder = find_folder(current_folder.path + "/" + line.split(" ")[2], current_folder.subfolders)
            if debug_level > 1:
                print("Moving from", current_folder, "to", next_folder)
            current_folder = next_folder
    #esentailly we don't give a shit about ls
    elif "$ ls" in line:
        # print("ls")
        continue
    #otherwise, we're parsing listed folders and files, basically just creating them
    else:
        if line.startswith("dir"):
            new_folder = Folder(line.split(" ")[1], current_folder)
            current_folder.subfolders.append(new_folder)
            if debug_level > 1:
                print("Created folder", new_folder, "in", current_folder)
        else:
            new_file = File(int(line.split(" ")[0]), line.split(" ")[1])
            current_folder.files.append(new_file)
            if debug_level > 1:
                print("Created file", new_file, "in", current_folder)
    if debug_level > 1:
        print("")

total_size = 0
root_folder.calculate_size(100000)
print("SIZE: " + str(total_size))