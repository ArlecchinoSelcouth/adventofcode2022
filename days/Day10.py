import argparse
import time

def generate_cycle_stack(program):
    stack = []
    for instruction in program:
        if "noop" in instruction:
            stack.append("noop")
        else:
            stack.append(instruction.strip())
            stack.append("processing")
    return stack


def part_one(input_data, debug):
    print("---------------------DAY 10 - PART ONE---------------------\n")

    register_value = 1
    register_sum = 0
    wanted_cycles = [20, 60, 100, 140, 180, 220]
    stack = generate_cycle_stack(input_data)

    for cmd in range(len(stack)):
        sum = register_value * (cmd+1) if cmd+1 in wanted_cycles else 0
        register_sum += sum
        if debug:
            print("{nl}@ cycle [{cycle:3d}] --> command [{cmd:10s}] --> stack [{stack:3d}]{sum}".format(
                cmd=stack[cmd],
                cycle=cmd+1,
                stack=register_value,
                nl="\n\t" if cmd+1 in wanted_cycles else "",
                sum="---> SUM [{s}]\n".format(
                    s=sum
                )   if cmd+1 in wanted_cycles else ""
            ))
        if "processing" in stack[cmd]:
            register_value += int(stack[cmd-1].split(" ")[1])

    print("\nDAY 10 - PART ONE - RESULT - [{result}]\n".format(
        result=register_sum
    ))

    return register_sum


def draw_sprite(sprite):
    line = (
        "." * (
            (sprite-2) if sprite > 1 else 0
        )
    ) + "###" + (
        "." * (
            40 - (sprite+1) if sprite > 1 else 40 - (sprite+2)
        )
    )
    return line


def part_two(input_data, debug):
    print("---------------------DAY 10 - PART TWO---------------------\n")

    stack = generate_cycle_stack(input_data)
    sprite = 1
    sprite_draw = "###" + "." * 37
    crt = ""
    register_value = 1

    for cycle in range(len(stack)):
        time.sleep(0.1)
        if debug:
            print("[DEBUG]")
            print("\tSprite [{s0:2d}, {s1:2d}, {s2:2d}] position: {crt} [{len}] @ cycle [{cycle}]".format(
                crt=sprite_draw,
                s0=sprite-1,
                s1=sprite,
                s2=sprite+1,
                len=len(sprite_draw),
                cycle=cycle+1
            ))

        crt += "#" if cycle % 40 in (sprite-1, sprite, sprite+1) else "."
        crt += "\n\t\t\t\t      " if (cycle+1) % 40 == 0 else ""
        
        if debug:
            print("\t\t\t  CRT screen: {crt}".format(
                crt=crt
            ))

        if "processing" in stack[cycle]:
            
            value = int(stack[cycle-1].split(" ")[1])
            sprite += value
            register_value += value

            #flatten sprite values to keep him in CRT bounds
            sprite = sprite if sprite >= 1 else 1
            sprite = sprite if sprite <= 39 else 39
            sprite = register_value if register_value >= 1 else sprite
            sprite = register_value if register_value <= 39 else sprite

            sprite_draw = draw_sprite(sprite)
        
        if debug:
            print("[DEBUG] Sprite vs Register value: [{sprite} {rel} {reg}]".format(
                sprite=sprite,
                reg=register_value,
                rel="==" if sprite == register_value else "!="
            ))
            

    print("\nDAY 10 - PART TWO - RESULT - [{result}]\n".format(
        result=2
    ))

    return 2


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file")
    parser.add_argument("-d", "--debug", action="store_true")
    parser.add_argument("-t", "--test", action="store_true")
    args = parser.parse_args()

    with open(args.file, "r") as file:
        input_data = file.readlines()

    result_1 = part_one(input_data, args.debug)
    result_2 = part_two(input_data, args.debug)

    if args.test:
        assert(result_1 == 13140)
        assert(result_2 == 2)
    
    print("===========================================================\n")
