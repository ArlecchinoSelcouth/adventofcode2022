import argparse

PRIORITIES = {}
value = 1

#creating a dictionary to assign each letter their priority value
#lowercase first (ascii from 97 to 122+1)
for letter in range(97, 123):
    PRIORITIES[chr(letter)] = value
    value += 1

#uppercase second (ascii from 65 to 90+1)
for letter in range(65, 91):
    PRIORITIES[chr(letter)] = value
    value += 1


def part_one(input_data, debug):
    print("---------------------DAY 03 - PART ONE---------------------\n")

    list_of_fuckups = []
    
    for backpack in input_data:
        backpack = backpack.strip()
        midpoint = int(len(backpack) / 2)
        first_half = set(backpack[:midpoint])
        second_half = set(backpack[midpoint:])

        for character in first_half:
            if character in second_half:
                list_of_fuckups.append(character)
                if(debug):
                    print("[DEBUG] in backpack \n\t\t{backpack}\n\tfound mistake: {mistake} with priority {priority}".format(
                        backpack=backpack,
                        mistake=character,
                        priority=PRIORITIES[character]
                    ))

    fuckup_priority_total = 0
    for fuckup in list_of_fuckups:
        fuckup_priority_total += PRIORITIES[fuckup]

    print("\nDAY 03 - PART ONE - RESULT - [{result}]\n".format(
        result=fuckup_priority_total
    ))

    return fuckup_priority_total



def part_two(input_data, debug):
    print("---------------------DAY 03 - PART TWO---------------------\n")

    badges_priority_total = 0
    
    for backpack in range(0, len(input_data), 3):
        for item in set(input_data[backpack].strip()):
            if item in input_data[backpack+1].strip() and item in input_data[backpack+2].strip():
                badges_priority_total += PRIORITIES[item]
                if(debug):
                    print("[DEBUG] In backpacks \n\t\t{b1}\t\t{b2}\t\t{b3}\tfound badge {badge} with priority {priority}".format(
                        b1=input_data[backpack],
                        b2=input_data[backpack+1],
                        b3=input_data[backpack+2],
                        badge=item,
                        priority=PRIORITIES[item]
                    ))

    print("\nDAY 03 - PART TWO - RESULT - [{result}]\n".format(
        result=badges_priority_total
    ))

    return badges_priority_total


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file")
    parser.add_argument("-d", "--debug", action="store_true")
    parser.add_argument("-t", "--test", action="store_true")
    args = parser.parse_args()

    with open(args.file, "r") as file:
        input_data = file.readlines()

    result_1 = part_one(input_data, args.debug)
    result_2 = part_two(input_data, args.debug)

    if args.test:
        assert(result_1 == 157)
        assert(result_2 == 70)
    
    print("===========================================================\n")
